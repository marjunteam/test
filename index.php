<!DOCTYPE html>
<html>
<head>
	<title></title>
	<link rel="stylesheet" href="css/bootstrap.css" >
	<link rel="stylesheet" href="css/style.css" >
	<script src="js/bootstrap.min.js"  ></script>
	<script src="js/jquery-1.12.1.min.js"  ></script>
	<script>
		
		$(document).ready(function(){
			
			$("#form").submit(function(e){
				errors = [];
				if($('[name="id"]').val() == ''){
					errors.push('Personal Identification is required');
				}else{
					var id = $('[name="id"]').val();
					if(id.length != 7){
						errors.push('Personal Identification should be 7 digits');
					}
				}
				if($('[name="fname"]').val() == ''){
					errors.push('First Name is required');
				}
				if($('[name="lname"]').val() == ''){
					errors.push('Last Name is required');
				}
				if(!$('[name="gender"]').is(":checked")){
					errors.push('Gender is required');
				}


				
				if(errors.length > 0){
					var strErrors = errors.toString();
					
					$(".result").html('<div class="alert alert-warning" role="alert"><strong>Please check errors listed below </strong><br/> ' + strErrors + '</div>');
					e.preventDefault();
				}

				
			});
		

			$.get("ajaxcall.php", function(data, status){
		        if(status == 'success'){
		        	var data = JSON.parse(data);
		        	$("#country").html('<option> - Select Country - </option>' + data);

		        }else{
		        	alert('No Data Country Found');
		        }
		    });


			
		});

	</script>
</head>
<body>
<div class="row wrapper">

<div class="result">
	<?php echo ($_GET['status'] == 'success') ? '<div class="alert alert-success fade in">
   
    <strong>Success!</strong> Guest successfully created!.
</div>' : '' ?>

</div>
	<form id="form" action="create_user.php" method="POST">
	<div class="form-group">
	    <label for="fname">Personal Identification</label>
	    <input type="text" class="form-control" id="id" name="id">
	  </div>
	  <div class="form-group">
	    <label for="fname">First Name</label>
	    <input type="texxt" class="form-control" id="fname" name="fname">
	  </div>
	  <div class="form-group">
	    <label for="lname">Last Name</label>
	    <input type="text" class="form-control" id="lname" name="lname">
	  </div>
	  <div class="form-group">
      <label for="disabledSelect">Title</label>
      <select id="areyou" name="areyou" class="form-control">
        <option value="Mr">Mr</option>
         <option value="Mrs">Mrs</option>
          <option value="Ms">Ms</option>
           <option value="Miss">Miss</option>
      </select>

      </div>
       <div class="form-group">

      <label class="radio-inline">
		  <input type="radio" name="gender" id="male" value="Male"> Male
		</label>
		<label class="radio-inline">
		  <input type="radio" name="gender" id="female" value="Female"> Female
		</label>
	</div>
	 <div class="form-group">	
		<label for="disabledSelect">Citizenship</label>
		<select id="country" name="country" class="form-control"></select>

    </div>

  	  <div class="form-group">
	    <label for="lname">Comment</label>
	    <textarea class="form-control" name="comment" id="comment"></textarea>
	  </div>
	
	
	  <button type="submit" class="btn btn-submit">Submit</button>
	  <button type="submit" class="btn btn-reset">Reset</button>
	</form>
 </div>

</body>
</html>