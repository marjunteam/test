<?php
	
	$servername = "localhost";
	$username = "root";
	$password = "";
	$dbname = "test";

	// Create connection
	$conn = new mysqli($servername, $username, $password, $dbname);
	// Check connection
	//Output any connection error
	if ($conn->connect_error) {
	    die('Error : ('. $conn->connect_errno .') '. $conn->connect_error);
	}

?>