<?php
require 'database.php';

//MySqli Select Query
$result = $conn->query("SELECT * FROM country");

while($row = $result->fetch_array())
{
	$rows[] = ['id' => $row['id'], 'code' => $row['country_code'], 'name' => $row['country_name']];
}

foreach ($rows as $key => $value) {
	$data[] = '<option value="' . $value['id'] . '">' . $value['code'] . ' - ' . $value['name'] . '</option>';
}

echo json_encode(implode('', $data));


/* free result set */
$result->close();

?>